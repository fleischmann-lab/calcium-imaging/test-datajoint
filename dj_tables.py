import datajoint as dj
import numpy as np
import os
from skimage import io

username = dj.conn().conn_info["user"]
schema = dj.schema(f"{username.replace('u_', '')}_koulakov", locals())


# Table definitions
@schema
class Mouse(dj.Manual):
    definition = """
    # Experimental animals
    mouse_id             : int                          # Unique animal ID
    ---
    dob=null             : date                         # date of birth
    sex="unknown"        : enum('M','F','unknown')      # sex
    """


@schema
class Session(dj.Manual):
    definition = """
    # Experiment session
    -> Mouse
    session_date               : date                         # date
    ---
    experiment_setup           : int                          # experiment setup ID
    experimenter               : varchar(100)                 # experimenter name
    data_path=''               : varchar(255)                 #
    """


@schema
class Scan(dj.Manual):
    definition = """
    -> Session
    scan_idx    : int           # scan index
    ---
    depth       : float         # depth of this scan
    wavelength  : float         # wavelength used
    laser_power : float         # power of the laser used
    fps         : float         # frames per second
    file_name    : varchar(128) # name of the tif file
    """


@schema
class AverageFrame(dj.Imported):
    definition = """
    -> Scan
    ---
    average_frame   : longblob     # average fluorescence across frames
    """

    def make(
        self, key
    ):  # key is the primary key of one of the entries in the table `Scan`
        # fetch data directory from table Session
        data_path = (Session & key).fetch1("data_path")

        # fetch data file name from table Scan
        file_name = (Scan & key).fetch1("file_name")

        # load the file
        im = io.imread(os.path.join(data_path, file_name))
        # compute the average image across the frames
        avg_image = np.mean(im, axis=0)

        # Now prepare the entry as a dictionary with all fields defined in the table.
        key["average_frame"] = avg_image  # inherit the primary key from the table Scan

        # insert entry with the method `insert1()`
        self.insert1(key)

        print("\tPopulated Scan {mouse_id} - {session_date} - {scan_idx}".format(**key))


@schema
class SegmentationParam(dj.Lookup):
    definition = """
    seg_param_id        : int      # unique id for cell segmentation parameter set
    ---
    threshold           : float
    size_cutoff         : float
    """


@schema
class Segmentation(dj.Computed):
    definition = """
    -> AverageFrame
    -> SegmentationParam
    ---
    segmented_masks         : longblob   # overview of segmented masks
    """

    class Roi(dj.Part):
        definition = """
        -> master
        roi_idx             : int        # index of an roi
        ---
        mask                : longblob   # mask of this roi
        """

    def make(self, key):  # key is one of the primary keys of the join product
        # of AverageFrame and ParameterSet

        print("Populating for: ", key)

        # fetch average image from the previous table AverageFrame
        avg_image = (AverageFrame & key).fetch1("average_frame")

        # fetch the parameters threshold and size_cutoff
        threshold, size_cutoff = (SegmentationParam & key).fetch1(
            "threshold", "size_cutoff"
        )

        # perform the thresholding and blob detection
        mask = avg_image > threshold
        label_im, nb_labels = ndimage.label(mask)
        sizes = np.array([np.sum(label_im == i) for i in np.unique(label_im)])

        small_size_filter = sizes < size_cutoff
        pixel_to_remove = small_size_filter[label_im]

        label_im[pixel_to_remove] = 0

        rois = []
        for i in np.unique(label_im)[1:]:  # 0 is the background
            rois.append(
                dict(
                    **key,  # inherit primary key from master table
                    roi_idx=i,
                    mask=label_im == i,
                )
            )

        # insert into the master table first
        self.insert1(dict(**key, segmented_masks=label_im))
        print("Detected {} ROIs!\n".format(len(rois)))
        # then insert into the part table
        self.Roi.insert(rois)


@schema
class Fluorescence(
    dj.Imported
):  # imported table because it also rely on the external tiff file.
    definition = """
    -> Segmentation
    ---
    time    : longblob    # time for each frame
    """

    class Trace(dj.Part):
        definition = """
        -> master
        -> Segmentation.Roi
        ---
        trace      :  longblob    # fluorescence trace of each ROI
        """

    # the master table is mainly to perform the computation,
    # while the part table contains the result
    def make(self, key):

        print("Populating: {}".format(key))
        # fetch data directory from table Session
        data_path = (Session & key).fetch1("data_path")

        # fetch data file name from table Scan
        file_name = (Scan & key).fetch1("file_name")

        # load the file
        im = io.imread(os.path.join(data_path, file_name))

        # get dimensions of the image and reshape
        n, w, h = np.shape(im)
        im_reshaped = np.reshape(im, [n, w * h])

        # get frames per second to compute time
        fps = (Scan & key).fetch1("fps")

        # insert into master table first
        self.insert1(dict(**key, time=np.array(range(n)) / fps))

        # extract traces
        roi_keys, masks = (Segmentation.Roi & key).fetch("KEY", "mask")

        traces = []
        for roi_key, mask in zip(roi_keys, masks):

            # reshape mask
            mask_reshaped = np.reshape(mask, [w * h])
            trace = np.mean(im_reshaped[:, mask_reshaped], axis=1)

            traces.append(dict(**roi_key, trace=trace))

        self.Trace.insert(traces)


dj.ERD(schema).draw()
dj.ERD(schema).save(os.path.join("schema", "schema.png"))
